<?php

/*
 * @author Balaji
 * Language File: English
 * @copyright � 2015 ProThemes.Biz
 *
 */
 
$lang = array();
 
$lang['1'] = "Home";
$lang['2'] = "Blog";
$lang['3'] = "Contact US";
$lang['4'] = "Malformed Request!";
$lang['5'] = "Your image verification code is wrong!";
$lang['6'] = "Paste (Ctrl + V) your article below then click Submit to watch this article rewriter do it's thing! ";
$lang['7'] = "Image Verification";
$lang['8'] = "Submit";
$lang['9'] = "Try New Document";
$lang['10'] = "Output";
$lang['11'] = "About";
$lang['12'] = "Try Again";
$lang['13'] = "Given Text";
$lang['14'] = "MD5 Hash";
$lang['15'] = "Your IP";
$lang['16'] = "City";
$lang['17'] = "Region";
$lang['18'] = "Country";
$lang['19'] = "ISP";
$lang['20'] = "Latitude";
$lang['21'] = "Longitude";
$lang['22'] = "Country Code";
$lang['23'] = "Enter a URL";
$lang['24'] = "Page URL";
$lang['25'] = "Page Size(Bytes)";
$lang['26'] = "Page Size(KB)";
$lang['27'] = "Try New URL";
$lang['28'] = "Enter your domain name";
$lang['29'] = "Page URL";
$lang['30'] = "Meta Title";
$lang['31'] = "Meta Description";
$lang['32'] = "Meta Keywords";
$lang['33'] = "Select Screen Resolution";
$lang['34'] = "Pixels";
$lang['35'] = "Check";
$lang['36'] = "URL entered does not seem to be a dynamic URL";
$lang['37'] = "Type 1 - Single Page URL";
$lang['38'] = "Generated URL";
$lang['39'] = "Example URL";
$lang['40'] = "Create a .htaccess file with the code below";
$lang['41'] = "The .htaccess file needs to be placed in";
$lang['42'] = "Type 2 - Directory Type URL";
$lang['43'] = "Enter the text that you wish to encode or decode";
$lang['44'] = "Encoded URL";
$lang['45'] = "Decoded URL";
$lang['46'] = "Links";
$lang['47'] = "Count";
$lang['48'] = "Total Links";
$lang['49'] = "Internal Links";
$lang['50'] = "External Links";
$lang['51'] = "NoFollow Links";
$lang['52'] = "Links inside the current website";
$lang['53'] = "Link's URL";
$lang['54'] = "NoFollow/DoFollow";
$lang['55'] = "Links going to outside websites";
$lang['56'] = "Paste (Ctrl + V) your article below then click Check for Plagiarism! ";
$lang['57'] = "Check for Plagiarism";
$lang['58'] = "Total Words";
$lang['59'] = "Maximum";
$lang['60'] = "words limit per search";
$lang['61'] = 'Copy and paste your article here and click "Check for Plagiarism"';
$lang['62'] = "Enter up to 100 URLs (Each URL must be on separate line)";
$lang['63'] = "Try New Sites";
$lang['64'] = "Result";
$lang['65'] = "Status of each sites";
$lang['66'] = "URLs";
$lang['67'] = "HTTP Code";
$lang['68'] = "Response Time";
$lang['69'] = "Status";
$lang['70'] = "Enter your text/paragraph here";
$lang['71'] = "Count Words";
$lang['72'] = "Total Words";
$lang['73'] = "Total Characters";
$lang['74'] = "How popular is";
$lang['75'] = "Stats";
$lang['76'] = "Global Rank";
$lang['77'] = "Popularity at";
$lang['78'] = "Regional Rank";
$lang['79'] = "Backlinks";
$lang['80'] = "Traffic Rank";
$lang['81'] = "Search Engine Traffic";
$lang['82'] = "Price of each sites";
$lang['83'] = "URLs";
$lang['84'] = "Approximate Price";
$lang['85'] = "WHOIS DATA";
$lang['86'] = "Get WHOIS Data";
$lang['87'] = "Get Domain Age";
$lang['88'] = "Value";
$lang['89'] = "Domain";
$lang['90'] = "Domain Created Date";
$lang['91'] = "Domain Updated Date";
$lang['92'] = "Domain Expiry Date";
$lang['93'] = "Try New Domain";
$lang['94'] = "Get Page Ranks";
$lang['95'] = "Page Rank of each sites";
$lang['96'] = "PageRank";
$lang['97'] = "Something Went Wrong!";
$lang['98'] = "URL";
$lang['99'] = "Time Taken";
$lang['100'] = "CSS Links";
$lang['101'] = "Script Links";
$lang['102'] = "Image Links";
$lang['103'] = "Other Resource Links";
$lang['104'] = "Link Type";
$lang['105'] = "Load Time";
$lang['106'] = "Domain Geo Information";
$lang['107'] = "Domain IP";
$lang['108'] = "IP";
$lang['109'] = "Get Source Code";
$lang['110'] = "Source Code";
$lang['111'] = "Listed";
$lang['112'] = "Not Listed";
$lang['113'] = "Generate MetaTags";
$lang['114'] = "Site Title";
$lang['115'] = "Site Description";
$lang['116'] = "Site Keywords (Separate with commas)";
$lang['117'] = "Allow robots to index your website?";
$lang['118'] = "Yes";
$lang['119'] = "No";
$lang['120'] = "What type of content will your site display?";
$lang['121'] = "Allow robots to follow all links?";
$lang['122'] = "Title must be within 70 Characters";
$lang['123'] = "Description must be within 150 Characters";
$lang['124'] = "keywords1, keywords2, keywords3";
$lang['125'] = "What is your site primary language?";
$lang['126'] = "(Optional Meta Tags)";
$lang['127'] = "Search engines should revisit this page after";
$lang['128'] = "days";
$lang['129'] = "Author";
$lang['130'] = "Copy and paste into your site.";
$lang['131'] = "Generate sitemap";
$lang['132'] = "Modified date";
$lang['133'] = "Change frequency";
$lang['134'] = "Default priority";
$lang['135'] = "Do not include";
$lang['136'] = "Server response date";
$lang['137'] = "Todays date";
$lang['138'] = "Custom date";
$lang['139'] = "Enter a domain name";
$lang['140'] = "How many pages do I need to crawl?";
$lang['141'] = "Crawling";
$lang['142'] = "Links Found";
$lang['143'] = "Success";
$lang['144'] = "Error";
$lang['145'] = "Error, Try again later!";
$lang['146'] = "Processing";
$lang['147'] = "Pages contain backlink";
$lang['148'] = "Pagerank";
$lang['149'] = "Status";
$lang['150'] = "Default -  All Robots are";
$lang['151'] = "Allowed";
$lang['152'] = "Refused";
$lang['153'] = "Crawl-Delay";
$lang['154'] = "Default - No Delay";
$lang['155'] = "Sitemap";
$lang['156'] = "(leave blank if you don't have)";
$lang['157'] = "Search Robots";
$lang['158'] = "Same as Default";
$lang['159'] = "Restricted Directories";
$lang['160'] = "The path is relative to root and must contain a trailing slash";
$lang['161'] = "Create Robots.txt";
$lang['162'] = "Now, Create 'robots.txt' file at your root directory. Copy above text and paste into the text file.";
$lang['163'] = "Create robots.txt file ?";
$lang['164'] = "robots.txt generated by atozseotools.com";
$lang['165'] = "Enter your domain name";
$lang['166'] = "Find Keyword Position";
$lang['167'] = "Not Found within";
$lang['168'] = "Empty Request";
$lang['169'] = "position";
$lang['170'] = "Keyword field cannot be empty!";
$lang['171'] = "Keywords";
$lang['172'] = "Check Positions upto";
$lang['173'] = "Enter keywords in separate line";
$lang['174'] = "Example";
$lang['175'] = "Your Browser";
$lang['176'] = "Browser Version";
$lang['177'] = "Your OS";
$lang['178'] = "User Agent";
$lang['179'] = "Good";
$lang['180'] = "Bad - Not Redirecting!";
$lang['181'] = "Domain";
$lang['182'] = "WWW Redirect Status";
$lang['183'] = "Requested URL looks down!";
$lang['184'] = "Code to Text Ratio is";
$lang['185'] = "Text content size";
$lang['186'] = "Total HTML size";
$lang['187'] = "Host";
$lang['188'] = "Class C";
$lang['189'] = "Enter up to 40 Domains (Each Domain must be on separate line)";
$lang['190'] = "No Email Found!";
$lang['191'] = "Email Found!";
$lang['192'] = "Email";
$lang['193'] = "Google Indexed";
$lang['194'] = "Pages";
$lang['195'] = "Hosting Provider";
$lang['196'] = "Hosting Info";
$lang['197'] = "Safe Site";
$lang['198'] = "Not a harmfull site, but take care";
$lang['199'] = "Potentially harmful site";
$lang['200'] = "Unknown";
$lang['201'] = "Enter up to 20 URLs (Each URL must be on separate line)";
$lang['202'] = "Antivirus stats of each sites";
$lang['203'] = "Percentage";
$lang['204'] = "Total Keywords";
$lang['205'] = "Listed";
$lang['206'] = "Not Listed";
$lang['207'] = "Overall";
$lang['208'] = "SPAM Database Server";
$lang['209'] = "Moz access id missing on database!";
$lang['210'] = "Moz secret key missing on database!";
$lang['211'] = "MozRank";
$lang['212'] = "Page Authority Score";
$lang['213'] = "Domain Authority Score";
$lang['214'] = "Backlinks (As per Alexa)";
$lang['215'] = "Backlinks (As per Google)";
$lang['216'] = "Backlinks (As per Bing)";
$lang['217'] = "Screenshot of";
$lang['218'] = "No reverse domain name detected!";
$lang['219'] = "Reverse Domain Names";
$lang['220'] = "Domain Name";
$lang['221'] = "Failed extended and basic XML-RPC ping!";
$lang['222'] = "Enter your blog url";
$lang['223'] = "Enter your blog name";
$lang['224'] = "Enter your blog updated url";
$lang['225'] = "Enter your blog RSS feed url";
$lang['226'] = "Ping Server List";
$lang['227'] = "Email ID looks not valid!";
$lang['228'] = "All fields must be filled out";
$lang['229'] = "Message Sent Successfully";
$lang['230'] = "Captcha code is wrong!";
$lang['231'] = "Error - Try Again (Message Failed)";
$lang['232'] = "Contact Us";
$lang['233'] = "We value all the feedbacks received from our customers.";
$lang['234'] = "If you have any queries, comments, suggestions or have anything to talk about.";
$lang['235'] = "Name";
$lang['236'] = "Email ID";
$lang['237'] = "Subject";
$lang['238'] = "Message";
$lang['239'] = "Send Message";
$lang['240'] = "Enter your full name";
$lang['241'] = "Enter your email id";
$lang['242'] = "Enter your subject";
$lang['243'] = "Enter your message";
$lang['244'] = "Contact Form";
$lang['245'] = "Name (required)";
$lang['246'] = "E-mail (required)";
$lang['247'] = "Send";
$lang['248'] = "Redirecting to you index page...";
$lang['249'] = "Login Success.. Redirecting to you index page...";
$lang['250'] = "Almost signup process over. One step need to go..";
$lang['251'] = "Auto generated name";
$lang['252'] = "Set your Username";
$lang['253'] = "No thanks keep auto generated name";
$lang['254'] = "Username not vaild";
$lang['255'] = "Username already taken";
$lang['256'] = "Unable to post on database! Contact Support!";
$lang['257'] = "Username changed successfully";
$lang['258'] = "Username not vaild";
$lang['259'] = "Oauth Login System";
$lang['260'] = "Oh, no your account was banned! Contact Support..";
$lang['261'] = "There was an error on Oauth service!";
$lang['262'] = "Domain Age";
$lang['263'] = "Sign In";
$lang['264'] = "Sign Up";
$lang['265'] = "Oauth Login System";
$lang['266'] = "Logout";
$lang['267'] = "Sign in using social network";
$lang['268'] = "Sign in using Facebook";
$lang['269'] = "Sign in using Google";
$lang['270'] = "Sign in with your username";
$lang['271'] = "Username";
$lang['272'] = "Password";
$lang['273'] = "Forgot Password";
$lang['274'] = "Resend activation email";
$lang['275'] = "Email";
$lang['276'] = "Full Name";
$lang['277'] = "Sign up with your email address";
$lang['278'] = "Account Confirmation";
$lang['279'] = "Activation code successfully sent to your mail id";
$lang['280'] = "Email ID already verified!";
$lang['281'] = "Email ID not found!";
$lang['282'] = "Unable to post on database! Contact Support!";
$lang['283'] = "Password changed successfully and Sent to your mail";
$lang['284'] = "Password Reset";
$lang['285'] = "You are already logged in";
$lang['286'] = "Login Successful..";
$lang['287'] = "Oh, no your account was banned! Contact Support..";
$lang['288'] = "Oh, no account not verified";
$lang['289'] = "Oh, no password is wrong";
$lang['290'] = "Username not found";
$lang['291'] = "All fields must be filled out!";
$lang['292'] = "Username already taken";
$lang['293'] = "Email ID already registered";
$lang['294'] = "Your account was successfully registered";
$lang['295'] = "Username not valid! Username can't contain special characters..";
$lang['296'] = "Database Error";
$lang['297'] = "Login/Register";
$lang['298'] = "An activation email has been sent to your email address, Please also check your Junk/Spam Folders";
$lang['299'] = "Login to your Account";
$lang['300'] = "Register an account";
$lang['301'] = "Enter your email address";
$lang['302'] = "Options:";
$lang['303'] = "Account already verified...";
$lang['304'] = "Something Went Wrong! Contact Support!";
$lang['305'] = "Account verified successfully.. <br /> <br /> You can login now..";
$lang['306'] = "Verification code is wrong..";
$lang['307'] = "Username not found";
$lang['308'] = "Site is down for maintenance";
$lang['309'] = "We are currently down for maintenance";
$lang['310'] = "Maintenance Notice";
$lang['311'] = "Guest user limit is reached!";
$lang['312'] = "to use SEO tools anymore..";
$lang['313'] = "Login required to access this tool!";
$lang['314'] = 'Popular Tags';
$lang['315'] = 'Latest Tweets';
$lang['316'] = 'Links';
$lang['317'] = 'Search Engine Optimization';
$lang['318'] = 'Get Started';
$lang['319'] = 'More than 50 SEO Tools to keep track your SEO issues <br/> and help to improve the visibility of a website in search <br/> engines.';
$lang['320'] = 'Enter up to 20 Links (Each Links must be on separate line)';
$lang['321'] = 'Not Cached';
$lang['322'] = "Status Code";
$lang['323'] = "Broken Link";
$lang['324'] = "Okay";
$lang['325'] = "Enquiry";
$lang['326'] = "No Subject";
$lang['327'] = "Input Site is not valid!";
$lang['328'] = "Enter your keyword";
$lang['329'] = "Suggest Queries";
$lang['330'] = "Enter your domain names";
$lang['331'] = "Meta Content";
$lang['332'] = "H1 to H4 Tags";
$lang['333'] = "Tags";
$lang['334'] = "Readable Text Content";
$lang['335'] = "Indexable Links";
$lang['336'] = "It looks like your IP has already been used to register an account today!";
$lang['337'] = "Save As XML File";
$lang['338'] = "Save Sitemap File";
$lang['339'] = "Save the Screenshot";
?>
<?php

//Website Reviewer Addon - Language Part
//Note: Don't translate inside [ ] Square Brackets!
//HTML Tags allowed!

$lang['AN1'] = 'Title Tag';
$lang['AN2'] = 'Meta Description';
$lang['AN3'] = 'Meta Keywords';
$lang['AN4'] = 'More Information';
$lang['AN5'] = 'Easy to solve';
$lang['AN6'] = 'Little tough to solve';
$lang['AN7'] = 'Hard to solve';
$lang['AN8'] = 'No action necessary';
$lang['AN9'] = 'Processing';
$lang['AN10'] = 'Something went wrong!';
$lang['AN11'] = 'No Title';
$lang['AN12'] = 'No Description';
$lang['AN13'] = 'Length';
$lang['AN14'] = 'character(s)';
$lang['AN15'] = 'No Keywords';
$lang['AN16'] = 'Headings';
$lang['AN17'] = 'Google Preview';
$lang['AN18'] = 'Show More';
$lang['AN19'] = 'Show Less';
$lang['AN20'] = 'Alt Attribute';
$lang['AN21'] = 'We found [image-count] images on this web page';
$lang['AN22'] = 'Missing Alt Tag';
$lang['AN23'] = 'False';
$lang['AN24'] = 'True';
$lang['AN25'] = 'No Alt Tag Missing';
$lang['AN26'] = '[missing-alt-tag] ALT attributes are empty or missing.';
$lang['AN27'] = 'No ALT attributes are empty or missing.';
$lang['AN28'] = 'Keywords Cloud';
$lang['AN29'] = 'No keyword found!';
$lang['AN30'] = 'Keyword Consistency';
$lang['AN31'] = 'Keywords';
$lang['AN32'] = 'Freq';
$lang['AN33'] = 'Title';
$lang['AN34'] = 'Desc';
$lang['AN35'] = 'Text/HTML Ratio';
$lang['AN36'] = 'HTML to Text Ratio is';
$lang['AN37'] = 'Text content size';
$lang['AN38'] = 'Total HTML size';
$lang['AN39'] = 'bytes';
$lang['AN40'] = 'GZIP compression Test';
$lang['AN41'] = 'Your webpage is compressed from [total-size] KB to [compressed-size] KB ([percentage] % size savings)';
$lang['AN42'] = 'Wow! It\'s GZIP Enabled.';
$lang['AN43'] = 'Oh No! GZIP is not enabled.';
$lang['AN44'] = 'Your webpage size is [total-size] KB, could be compressed upto [compressed-size] KB using GZIP ([percentage] % size savings)';
$lang['AN45'] = 'WWW Resolve';
$lang['AN46'] = 'Great, a redirect is in place to redirect traffic from your non-preferred domain.';
$lang['AN47'] = 'Warning, no 301 redirects are in place to redirect traffic to your preferred domain. Pages that load successfully both with and without www. are treated as duplicate content!';
$lang['AN48'] = 'IP Canonicalization';
$lang['AN49'] = 'No your domain IP [ip] does not redirect to [host]';
$lang['AN50'] = 'Yes your domain IP [ip] redirected to [host]';
$lang['AN51'] = 'In-Page Links';
$lang['AN52'] = 'Internal Links';
$lang['AN53'] = 'External Links';
$lang['AN54'] = 'Anchor';
$lang['AN55'] = 'Type';
$lang['AN56'] = 'Follow';
$lang['AN57'] = 'We found a total of [count] links including both internal & external links of your site';
$lang['AN58'] = 'Broken Links';
$lang['AN59'] = 'XML Sitemap';
$lang['AN60'] = 'Robots.txt';
$lang['AN61'] = 'URL Rewrite';
$lang['AN62'] = 'Underscores in the URLs';
$lang['AN63'] = 'Embedded Objects';
$lang['AN64'] = 'Great, you are not using underscores (these_are_underscores) in your URLs';
$lang['AN65'] = 'Oh no, you are using underscores (these_are_underscores) in your URLs';
$lang['AN66'] = 'Good, all URLs look clean and friendly';
$lang['AN67'] = 'Warning! We have detected parameters in a massive number of URLs';
$lang['AN68'] = 'No broken links were found on this web page';
$lang['AN69'] = 'Broken links were found on this web page';
$lang['AN70'] = 'Good, you have XML Sitemap file!';
$lang['AN71'] = 'Oh no, XML Sitemap file not found!';
$lang['AN72'] = 'XML Sitemap Link';
$lang['AN73'] = 'Good, you have Robots.txt file!';
$lang['AN74'] = 'Oh no, Robots.txt file not found!';
$lang['AN75'] = 'Robots.txt Link';
$lang['AN76'] = 'Iframe';
$lang['AN77'] = 'Perfect, no embedded objects has been detected on this page';
$lang['AN78'] = 'Oh no, embedded objects has been detected on this page';
$lang['AN79'] = 'Perfect, no Iframe content has been detected on this page';
$lang['AN80'] = 'Oh no, iframe content has been detected on this page';
$lang['AN81'] = 'Domain Registration';
$lang['AN82'] = 'Loading...';
$lang['AN83'] = 'WHOIS Data';
$lang['AN84'] = 'WhoIs domain information can help you determine the proper administrator, billing and technical <br> contact information.';
$lang['AN85'] = 'Exactly how many years and months';
$lang['AN86'] = 'Domain Age';
$lang['AN87'] = 'Created Date';
$lang['AN88'] = 'Updated Date';
$lang['AN89'] = 'Expiry Date';
$lang['AN90'] = 'Not available for your domain';
$lang['AN91'] = 'Mobile Friendliness';
$lang['AN92'] = 'Mobile View';
$lang['AN93'] = 'Mobile Compatibility';
$lang['AN94'] = 'URL';
$lang['AN95'] = 'Favicon';
$lang['AN96'] = 'Custom 404 Page';
$lang['AN97'] = 'Page Size';
$lang['AN98'] = 'Load Time';
$lang['AN99'] = 'Language';
$lang['AN100'] = 'Domain Availability';
$lang['AN101'] = 'Typo Availability';
$lang['AN102'] = 'Email Privacy';
$lang['AN103'] = 'Safe Browsing';
$lang['AN104'] = 'Server IP';
$lang['AN105'] = 'Speed Tips';
$lang['AN106'] = 'Analytics';
$lang['AN107'] = 'W3C Validity';
$lang['AN108'] = 'Doc Type';
$lang['AN109'] = 'Encoding';
$lang['AN110'] = 'Indexed Pages';
$lang['AN111'] = 'Backlinks Counter ';
$lang['AN112'] = 'Social Data';
$lang['AN113'] = 'Estimated Worth';
$lang['AN114'] = 'Traffic Rank';
$lang['AN115'] = 'Visitors Localization';
$lang['AN116'] = 'Awesome! This page is mobile-friendly!';
$lang['AN117'] = 'Your mobile friendly score is [score]/100';
$lang['AN118'] = 'Oh No! This page is not mobile-friendly.';
$lang['AN119'] = 'No Screenshot available!';
$lang['AN120'] = 'Perfect, no embedded objects detected.';
$lang['AN121'] = 'Bad, embedded objects detected.';
$lang['AN122'] = '<b>Length:</b> [count] characters';
$lang['AN123'] = 'Great, your website has a favicon.';
$lang['AN124'] = 'Great, your website has a custom 404 error page.';
$lang['AN125'] = 'Bad, your website has no custom 404 error page. ';
$lang['AN126'] = '[size] KB (World Wide Web average is 320 Kb)';
$lang['AN127'] = '[time] second(s)';
$lang['AN128'] = 'Good, you have declared your language';
$lang['AN129'] = 'Oh no, you have not declared your language';
$lang['AN130'] = 'Declared Language: [language]';
$lang['AN131'] = 'Available';
$lang['AN132'] = 'Already Registered';
$lang['AN133'] = 'Query Failed';
$lang['AN134'] = 'Domains (TLD)';
$lang['AN135'] = 'Status';
$lang['AN136'] = 'Good, no email address has been found in plain text. ';
$lang['AN137'] = 'Email address has been found in plain text!';
$lang['AN138'] = 'The website is not blacklisted and looks safe to use.';
$lang['AN139'] = 'The website is blacklisted and not safe to use.';
$lang['AN140'] = 'Something went wrong on the server. Please try again.';
$lang['AN141'] = 'Server IP';
$lang['AN142'] = 'Server Location';
$lang['AN143'] = 'Service Provider';
$lang['AN144'] = 'Perfect, your website has few CSS files.';
$lang['AN145'] = 'Too bad, your website has too many CSS files.';
$lang['AN146'] = 'Perfect, your website has few JavaScript files.';
$lang['AN147'] = 'Too bad, your website has too many JavaScript files.';
$lang['AN148'] = 'Perfect, your website doesn\'t use nested tables.';
$lang['AN149'] = 'Too bad, your website is using nested tables.';
$lang['AN150'] = 'Perfect, your website doesn\'t use inline styles.';
$lang['AN151'] = 'Too bad, your website is using inline styles. ';
$lang['AN152'] = 'Tips for authoring fast-loading HTML pages:';
$lang['AN153'] = 'We didn\'t detect an analytics tool installed on this website.';
$lang['AN154'] = 'Perfect, We detect an analytics tool installed on this website.';
$lang['AN155'] = 'HTML doctype declaration is missing or is syntactically invalid!';
$lang['AN156'] = 'Your Web Page doctype is';
$lang['AN157'] = 'Yes, W3C Validated';
$lang['AN158'] = 'W3C not validated';
$lang['AN159'] = 'Great, language/character encoding is specified: ';
$lang['AN160'] = 'Oh no, language/character encoding is not specified!';
$lang['AN161'] = 'Indexed pages in search engines';
$lang['AN162'] = 'Page(s)';
$lang['AN163'] = 'Backlink(s)';
$lang['AN164'] = 'most visited website in the World.';
$lang['AN165'] = 'No Global Rank';
$lang['AN166'] = 'Number of backlinks to your website';
$lang['AN167'] = 'Your social media status';
$lang['AN168'] = 'Countries';
$lang['AN169'] = 'Percent of Visitors';
$lang['AN170'] = '<b>No data available</b>';
$lang['AN171'] = 'Your website is popular on following countries:';
$lang['AN172'] = 'End';

//Suggestion Box Messages

//Title Tag
$lang['AN173'] = 'Enter your suggestion message!'; 

//Meta Description
$lang['AN174'] = 'Enter your suggestion message!';

//Meta Keywords
$lang['AN175'] = 'Enter your suggestion message!';

//Headings
$lang['AN176'] = 'Enter your suggestion message!';

//Google Preview
$lang['AN177'] = 'Enter your suggestion message!';

//Alt Attribute
$lang['AN178'] = 'Enter your suggestion message!';

//Keywords Cloud
$lang['AN179'] = 'Enter your suggestion message!';

//Keyword Consistency
$lang['AN180'] = 'Enter your suggestion message!';

//Text/HTML Ratio
$lang['AN181'] = 'Enter your suggestion message!';

//GZIP compression Test
$lang['AN182'] = 'Enter your suggestion message!';

//WWW Resolve
$lang['AN183'] = 'Enter your suggestion message!';

//IP Canonicalization
$lang['AN184'] = 'Enter your suggestion message!';

//In-Page Links
$lang['AN185'] = 'Enter your suggestion message!';

//Broken Links
$lang['AN186'] = 'Enter your suggestion message!';

//Robots.txt
$lang['AN187'] = 'Enter your suggestion message!';

//XML Sitemap
$lang['AN188'] = 'Enter your suggestion message!';

//URL Rewrite
$lang['AN189'] = 'Enter your suggestion message!';

//Underscores in the URLs
$lang['AN190'] = 'Enter your suggestion message!';

//Embedded Objects
$lang['AN191'] = 'Enter your suggestion message!';

//Iframe
$lang['AN192'] = 'Enter your suggestion message!';

//Domain Registration
$lang['AN193'] = 'Enter your suggestion message!';

//WHOIS Data
$lang['AN194'] = 'Enter your suggestion message!';

//Mobile Friendliness
$lang['AN195'] = 'Enter your suggestion message!';

//Mobile View
$lang['AN196'] = 'Enter your suggestion message!';

//Mobile Compatibility
$lang['AN197'] = 'Enter your suggestion message!';

//URL
$lang['AN198'] = 'Enter your suggestion message!';

//Favicon
$lang['AN199'] = 'Enter your suggestion message!';

//Custom 404 Page
$lang['AN200'] = 'Enter your suggestion message!';

//Page Size
$lang['AN201'] = 'Enter your suggestion message!';

//Load Time
$lang['AN202'] = 'Enter your suggestion message!';

//Language
$lang['AN203'] = 'Enter your suggestion message!';

//Domain Availability
$lang['AN204'] = 'Enter your suggestion message!';

//Typo Availability
$lang['AN205'] = 'Enter your suggestion message!';

//Email Privacy
$lang['AN206'] = 'Enter your suggestion message!';

//Safe Browsing
$lang['AN207'] = 'Enter your suggestion message!';

//Server IP
$lang['AN208'] = 'Enter your suggestion message!';

//Speed Tips
$lang['AN209'] = 'Enter your suggestion message!';

//Analytics
$lang['AN210'] = 'Enter your suggestion message!';

//W3C Validity
$lang['AN211'] = 'Enter your suggestion message!';

//Doc Type
$lang['AN212'] = 'Enter your suggestion message!';

//Encoding
$lang['AN213'] = 'Enter your suggestion message!';

//Indexed Pages
$lang['AN214'] = 'Enter your suggestion message!';

//Backlinks Counter
$lang['AN215'] = 'Enter your suggestion message!';

//Social Data
$lang['AN216'] = 'Enter your suggestion message!';

//Estimated Worth
$lang['AN217'] = 'Enter your suggestion message!';

//Traffic Rank
$lang['AN218'] = 'Enter your suggestion message!';

//Visitors Localization
$lang['AN219'] = 'Enter your suggestion message!';

?>