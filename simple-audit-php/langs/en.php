<?php
/*
 * @author Balaji
 */

$lang = array();

// Index Page
$lang['1'] = 'Analyze your web pages with our'; #  Turbo SEO Analyzer 
$lang['2'] = 'and Build SEO reports as PDF.';
  
$lang['3'] = 'Please Wait... Analyzing your site...';
$lang['4'] = 'Enter your domain....';
$lang['5'] = 'Look-up big websites or your competitors…';

$lang['6'] = 'Optimize';
$lang['7'] = 'Optimize your site for organic search with better SEO.';
$lang['8'] = 'Responsive Design';
$lang['9'] = 'Fits many resolutions, from large desktops to small mobile devices.';
$lang['10'] = 'IP Blacklisted or Not';
$lang['11'] = 'Featuring over 100 DNS based blacklists & other spam details.';
$lang['12'] = 'Enhanced For Printing';
$lang['13'] = 'SEO report can be printable and offline use as well.';
$lang['14'] = 'Ranking';
$lang['15'] = 'Analyze your site with popular ranking system and optimize it.';
$lang['16'] = 'Social Stats';
$lang['17'] = 'Popularity & Detailed report of the social stats of a website.';
$lang['18'] = 'Many Plugins';
$lang['19'] = 'Over 10 plugins and an aditional 3 custom made plugins just for Turbo SEO.';
$lang['20'] = 'Popular Sites';

// Contact US
$lang['21'] = 'Alert!';
$lang['22'] = 'Message Sent Successfully.';
$lang['23'] = 'Error - Try Again (Message Failed)';
$lang['24'] = 'Enter your full name';
$lang['25'] = 'Enter your email id';
$lang['26'] = 'Enter your subject';
$lang['27'] = 'Message';
$lang['28'] = 'Discard';
$lang['29'] = 'Send Message';

// Banned User
$lang['30'] = 'Banned User';
$lang['31'] = 'Home';
$lang['32'] = 'Oops! Access Denied';
$lang['33'] = 'What happened?';
$lang['34'] = 'The owner of this website has banned your IP address';
$lang['35'] = 'Banned Site';
$lang['36'] = 'The owner of this website has banned your domain name';

// Recent Sites
$lang['37'] = 'Recent Sites';
$lang['38'] = 'Previous';
$lang['39'] = 'Next';

// SEO Result Page 
$lang['40'] = 'Not Available';
$lang['41'] = 'According to the Alexa.com, this site has';
$lang['42'] = 'rank in the world wide web. Low Alexa rank';
$lang['43'] = 'indicates that the site is visited by a lot of visitors and most popular in the Internet. This web site has';
$lang['44'] = 'Google PageRank';
$lang['45'] = 'out of 10 maxmium and is';
$lang['46'] = 'in DMOZ. The age of';
$lang['47'] = 'Overall site looking';
$lang['48'] = 'Visit website';
$lang['49'] = 'New Search';
$lang['50'] = 'Site Information';
$lang['51'] = 'Status';
$lang['52'] = 'Response Time';
$lang['53'] = 'Overall';
$lang['54'] = 'Your Score';
$lang['55'] = 'Generate report as<br /> PDF file';
$lang['56'] = 'Generate PDF';
$lang['57'] = 'Meta Tags';
$lang['58'] = 'Webpage Info';
$lang['59'] = 'Status';
$lang['60'] = 'Title';
$lang['61'] = 'Description';
$lang['62'] = 'Keywords';
$lang['63'] = 'Robot.txt file not present';
$lang['64'] = 'Robot.txt file present';
$lang['65'] = 'Sitemap file not present';
$lang['66'] = 'Sitemap file present';
$lang['67'] = 'General Information';
$lang['68'] = 'SEO stats';
$lang['69'] = 'Services';
$lang['70'] = 'Value';
$lang['71'] = 'Host Information';
$lang['72'] = 'Domain IP';
$lang['73'] = 'Country';
$lang['74'] = 'ISP';
$lang['75'] = 'Server IP Blacklist/NOT';
$lang['76'] = 'Blacklist means involved in spamming or other unwanted online behavior, on your server IP address.';
$lang['77'] = 'WHOIS Information';
$lang['78'] = 'Social Stats';

//Nav Bar
$lang['79'] = 'Home';
$lang['80'] = 'Recent Sites';
$lang['81'] = 'Contact US';

//Update v1.2
$lang['82'] = 'Crawling files';
$lang['83'] = 'Malware detection';
$lang['84'] = 'Detailed Info';
$lang['85'] = 'The website is not blacklisted and looks safe to use.';
$lang['86'] = 'Site is free from malware and other harmful codes';
$lang['87'] = 'The website is blacklisted and not safe to use.';
$lang['88'] = 'Malware and other harmful codes are detected';
$lang['89'] = 'Antivirus Check';
?>