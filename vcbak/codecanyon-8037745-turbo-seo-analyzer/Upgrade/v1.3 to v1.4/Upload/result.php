<?php

/*
* @author Balaji
* @name: Turbo SEO Analyzer PHP Script
* @copyright � 2014 ProThemes.Biz
*
*/


error_reporting(1);

require_once ('core/functions.php');
require_once ('core/whois.php');
require_once ('config.php');
$banned_ip = "0.0.0.0";
$ban_user = 1;
$banned_site = "example.com";
$ban_site = 1;
$title = $description = $keywords = "";
$con = mysqli_connect($mysql_host, $mysql_user, $mysql_pass, $mysql_database);

if (mysqli_connect_errno())
{
    $sql_error = mysqli_connect_error();
    $check_site = 0;
    goto myoff;
}
$query = "SELECT * FROM site_info";
$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result))
{
    $msite_name = Trim($row['site_name']);
    $mtwit = Trim($row['twit']);
    $mface = Trim($row['face']);
    $mgplus = Trim($row['gplus']);
    $ex_1 = Trim($row['ex_1']);
    $ex_2 = Trim($row['ex_2']);
    $ps_1 = Trim($row['ps_1']);
    $ps_2 = Trim($row['ps_2']);
    $ps_3 = Trim($row['ps_3']);
    $ga = Trim($row['ga']);
}

$sql = "SELECT * FROM lang where id='0'";
$result = mysqli_query($con, $sql);

while ($row = mysqli_fetch_array($result))
{
    //populate and display results data in each row
    $lang = Trim($row['type']);
}
require_once ("langs/$lang");


$query = "SELECT * FROM ads WHERE id='1'";
$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result))
{
    $text_ads = Trim($row['text_ads']);
    $ads_1 = Trim($row['ads_1']);
    $ads_2 = Trim($row['ads_2']);

}

?>

<?php

if ($_SERVER['REQUEST_METHOD'] == POST)
{
    $my_site = "1";
    $score = "0";
    $date = date('jS F Y');
    $ip = $_SERVER['REMOTE_ADDR'];
    $site = Trim(htmlentities($_POST['url']));
    $site = clean_url($site);
    $res = isValidSite($site);
    if ($res == '1')
    {
        $check_site = 0;
        goto myoff;
    }
    $wsite = "www.$site";

    $query = "SELECT * FROM ban_user";
    $result = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($result))
    {
        $banned_ip = $banned_ip . "::" . $row['ip'];
    }
    if (strpos($banned_ip, $ip) !== false)
    {
        $ban_user = 0;
        goto banned_user;
    }

    $query = "SELECT * FROM ban_site";
    $result = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($result))
    {
        $banned_site = $banned_site . "::" . $row['site'];
    }
    if (strpos($banned_site, $site) !== false)
    {
        $ban_site = 0;
        goto banned_site;
    }

    $dir = 'temp';
    $files1 = scandir($dir);
    $myimagedata = "";
    $dircount = count($files1);

    for ($loop = 0; $loop <= $dircount; $loop++)
    {
        $myimagedata = "$myimagedata $files1[$loop]";
    }


    if (strpos($myimagedata, "$site.jpg") == true)
    {
        $myimage = "temp/$site.jpg";
    } else
    {
        $name = "temp/$site.jpg";
        $imgSrc = file_get_contents("http://lic.prothemes.biz/api/tscreen.php?site=$site&domain=".$_SERVER['HTTP_HOST']."&code=$item_purchase_code");
        $myFile = $name;
        $fh = fopen($myFile, 'w') or die("Can't open file");
        $stringData = $imgSrc;
        fwrite($fh, $stringData);
        fclose($fh);
        $ssimage = imagecreatefromjpeg($myFile);

        if ($imgSrc == "")
        {
            unlink($name);
            $myimage = "img/no-preview.png";
        } else
        {
            $myimage = "temp/$site.jpg";
            $name = $myimage;
            $thumb_width = 1024;
            $thumb_height = 768;

            $width = imagesx($ssimage);
            $height = imagesy($ssimage);

            $original_aspect = $width / $height;
            $thumb_aspect = $thumb_width / $thumb_height;

            if ($original_aspect >= $thumb_aspect)
            {
                // If image is wider than thumbnail (in aspect ratio sense)
                $new_height = $thumb_height;
                $new_width = $width / ($height / $thumb_height);
            } else
            {
                // If the thumbnail is wider than the image
                $new_width = $thumb_width;
                $new_height = $height / ($width / $thumb_width);
            }

            $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

            $co = imagecolorallocate($thumb, 241, 241, 241);
            imagefill($thumb, 0, 0, $co);
            $text_color = imagecolorallocate($thumb, 153, 153, 153);
            imagestring($thumb, 200, 400, 300, 'No Preview Available', $text_color);

            // Resize and crop
            imagecopyresampled($thumb, $ssimage, 0,
                //- ($new_width - $thumb_width) / 2, // Center the image horizontally
                0, // - ($new_height - $thumb_height) / 2, // Center the image vertically
                0, 0, $new_width, $new_height, $width, $height);
            imagejpeg($thumb, $myimage, 80);

            if (filesize($name) == 0)
            {
                unlink($name);
                $myimage = "img/no-preview.png";
            } elseif (filesize($name) <= 4)
            {
                unlink($name);
                $myimage = "img/no-preview.png";
            } else
            {
                $myimage = "temp/$site.jpg";
            }
        }
    }


    $go_rank = google_page_rank($wsite);
    if ($go_rank == "")
        $go_rank = 0;
    $alexa = alexaRank($wsite);
    $sitemap_r = sitemap_check($site);
    $robo_r = robocheck($site);

    $whois = new Whois;
    $whois_data = $whois->whoislookup($wsite);
    $age = new DomainAge;
    $age = $age->age($wsite);
    if ($age == '')
        $age = $lang['40'];

    if (checkOnline($wsite))
    {
        $vtime = $rtime['total_time'];
        $html = file_get_contents("http://" . $wsite);
        $html = str_ireplace(array("Title", "TITLE"), "title", $html);
        $html = str_ireplace(array("Description", "DESCRIPTION"), "description", $html);
        $html = str_ireplace(array("Keywords", "KEYWORDS"), "keywords", $html);
        $html = str_ireplace(array("Content", "CONTENT"), "content", $html);
        $html = str_ireplace(array("Meta", "META"), "meta", $html);
        $html = str_ireplace(array("Name", "NAME"), "name", $html);

        $doc = new DOMDocument();
        @$doc->loadHTML($html);
        $nodes = $doc->getElementsByTagName('title');

        $title = $nodes->item(0)->nodeValue;

        $metas = $doc->getElementsByTagName('meta');

        for ($i = 0; $i < $metas->length; $i++)
        {
            $meta = $metas->item($i);
            if ($meta->getAttribute('name') == 'description')
                $description = $meta->getAttribute('content');
            if ($meta->getAttribute('name') == 'keywords')
                $keywords = $meta->getAttribute('content');
        }
        if ($title == '')
            $title = "No Title";
        if ($description == '')
            $description = "No Description";
        if ($keywords == '')
            $keywords = "No Keywords";
        $status = "Online";
    } else
    {
        $title = "No Title";
        $vtime = "No response";
        $status = "Offline";
        $my_site = "0";
        goto off;
    }

    $query = "INSERT INTO user_history (last_date,ip,site) VALUES ('$date','$ip','$site')";

    mysqli_query($con, $query);

    $obj = new socialCount($wsite);
    $Tweets = $obj->get_tweets();
    $facebook = $obj->get_fb();
    $facebook = explode("::", $facebook);
    $face_like = $facebook[1];
    $face_share = $facebook[0];
    $face_come = $facebook[2];
    $LinkedIn = $obj->get_linkedin();
    $PlusOnes = $obj->get_plusones();
    $Delicious = $obj->get_delicious();
    $StumbleUpon = $obj->get_stumble();


    $data = host_info($wsite);
    $data = explode("::", $data);

    $host_ip = $data[0];
    $host_country = $data[1];
    $host_isp = $data[2];

    $goglebacklink = googleBack($site);
    $googleindexp = googleIndex($site);
    $bingbacklink = bingBack($site);
    $dmoz = dmozCheck($site);

    $res = dnsblookup(Trim($data[0]));

    $black_check = "Your server IP($data[0]) is $res.";

    $black_data = $res;

    $dir = 'temp';
    $files1 = scandir($dir);
    $myimagedata = "";
    $dircount = count($files1);

    $alexa_score = str_replace(array(
        ",",
        ".",
        ":",
        "'",
        " "), "", $alexa);

    if ($alexa_score < 1000)
        $score = $score + 5;
    elseif ($alexa_score < 40000)
        $score = $score + 4;
    elseif ($alexa_score < 60000)
        $score = $score + 3;
    elseif ($alexa_score < 80000)
        $score = $score + 2;
    else
        $score = $score + 1;

    $go_rank = Trim($go_rank);

    if ($go_rank > 8)
        $score = $score + 5;
    elseif ($go_rank > 6)
        $score = $score + 4;
    elseif ($go_rank > 4)
        $score = $score + 3;
    elseif ($go_rank > 2)
        $score = $score + 2;
    elseif ($go_rank > 1)
        $score = $score + 1;
    else
        $score = $score;


    $res_s = explode(".", $vtime);
    $res_s = Trim($res_s[0]);

    if ($res_s < 1)
    {
        $score = $score + 10;
    } elseif ($res_s < 2)
    {
        $score = $score + 8;
    } elseif ($res_s < 3)
    {
        $score = $score + 7;
    } elseif ($res_s < 4)
    {
        $score = $score + 6;
    } elseif ($res_s < 5)
    {
        $score = $score + 5;
    } elseif ($res_s < 6)
    {
        $score = $score + 4;
    } elseif ($res_s < 7)
    {
        $score = $score + 3;
    } else
    {
        $score = $score + 1;
    }


    if ($title == "No Title")
    {
    } elseif ($title == "")
    {

    } else
    {
        if (strlen($title) > "71")
        {
        } elseif (strlen($title) < "10")
        {
            $score = $score + 2;
        } elseif (strlen($title) < "30")
        {
            $score = $score + 4;
        } else
        {
            $score = $score + 10;
        }
    }


    if ($description == "No Description")
    {

    } elseif ($description == "")
    {
    } else
    {
        if (strlen($description) > "170")
        {
            $score = $score + 1;

        } elseif (strlen($description) < "10")
        {
            $score = $score + 4;
        } else
        {
            $score = $score + 7;
        }
    }

?>

<?php

    if (strpos($age, "years") == true)
    {
        $age_s = explode("years", $age);
        $age_s = Trim($age_s[0]);
        if ($age_s == "")
            $age_s = 0;
    } else
    {
        $age_s = 0;
    }


    if ($age_s < 1)
    {
        $score = $score + 1;
    } elseif ($age_s < 2)
    {
        $score = $score + 4;
    } elseif ($age_s < 3)
    {
        $score = $score + 6;
    } elseif ($age_s < 6)
    {
        $score = $score + 8;
    } else
    {
        $score = $score + 10;
    }
    if ($keywords == "No Keywords")
    {
    } else
    {
        if (str_word_count($keywords) > "6")
        {

        } elseif (strlen($keywords) < "1")
        {
            $score = $score + 2;
        } else
        {
            $score = $score + 3;
        }
    }

    $goglebacklink_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $goglebacklink);
    if ($goglebacklink_s < 10)
    {
    } elseif ($goglebacklink_s < 20)
    {
    } elseif ($goglebacklink_s < 40)
    {
        $score = $score + 1;
    } elseif ($goglebacklink_s < 50)
    {
        $score = $score + 1;
    } elseif ($goglebacklink_s < 100)
    {
        $score = $score + 1;
    } elseif ($goglebacklink_s < 200)
    {
        $score = $score + 2;
    } else
    {
        $score = $score + 3;
    }

    $googleindexp_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $googleindexp);
    if ($googleindexp_s < 20)
    {
    } elseif ($googleindexp_s < 30)
    {
    } elseif ($googleindexp_s < 50)
    {
        $score = $score + 1;
    } elseif ($googleindexp_s < 100)
    {
        $score = $score + 1;
    } elseif ($googleindexp_s < 200)
    {
        $score = $score + 1;
    } elseif ($googleindexp_s < 400)
    {
        $score = $score + 2;
    } else
    {
        $score = $score + 3;
    }
    $mal_value = Trim(check_mal($site));

    if ($mal_value == "204")
    {
        $score = $score + 3;
    }
    $bingbacklink_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $bingbacklink);
    if ($bingbacklink_s < 5)
    {
    } elseif ($bingbacklink_s < 15)
    {
    } elseif ($bingbacklink_s < 30)
    {
        $score = $score + 1;
    } elseif ($bingbacklink_s < 40)
    {
        $score = $score + 1;
    } elseif ($bingbacklink_s < 50)
    {
        $score = $score + 1;
    } elseif ($bingbacklink_s < 100)
    {
        $score = $score + 2;
    } else
    {
        $score = $score + 3;
    }

    if ($dmoz == "Listed")
    {
        $score = $score + 4;
    } else
    {
    }
    if (trim($black_data) == "Not Blacklist")
    {
        $score = $score + 10;
    } else
    {
        $score = $score + 2;
    }


    $face_like_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $face_like);
    if ($face_like_s < 100)
    {
        $score = $score + 1;
    } elseif ($face_like_s < 300)
    {
        $score = $score + 2;
    } elseif ($face_like_s < 500)
    {
        $score = $score + 3;
    } elseif ($face_like_s < 1000)
    {
        $score = $score + 5;
    } else
    {
        $score = $score + 6;
    }


    $face_share_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $face_share);
    if ($face_share_s < 60)
    {
        $score = $score + 1;
    } else
    {
        $score = $score + 2;
    }
    $face_come_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $face_come);
    if ($face_come_s < 50)
    {
        $score = $score + 1;
    } else
    {
        $score = $score + 2;
    }

    $PlusOnes_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $PlusOnes);

    if ($PlusOnes_s < 100)
    {
        $score = $score + 1;
    } elseif ($PlusOnes_s < 200)
    {
        $score = $score + 2;
    } elseif ($PlusOnes_s < 300)
    {
        $score = $score + 2;
    } else
    {
        $score = $score + 4;
    }
    $Delicious_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $Delicious);
    if ($Delicious_s = "")
    {
    } else
    {
        if ($Delicious_s == 0)
        {
        } else
        {
            $score = $score + 1;
        }
    }

    $StumbleUpon_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $StumbleUpon);
    if ($StumbleUpon_s = "")
    {
    } else
    {
        if ($StumbleUpon_s == 0)
        {
        } else
        {
            $score = $score + 1;
        }
    }

    $Tweets_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $Tweets);

    if ($Tweets_s < 100)
    {
        $score = $score + 1;
    } elseif ($Tweets_s < 200)
    {
        $score = $score + 2;
    } elseif ($Tweets_s < 300)
    {
        $score = $score + 2;
    } else
    {
        $score = $score + 4;
    }


    $LinkedIn_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $LinkedIn);

    if ($LinkedIn_s = "")
    {
    } else
    {
        if ($LinkedIn_s == 0)
        {
        } else
        {
            $score = $score + 1;
        }
    }

?>
 
           <?php

    if ($score < 10)
    {
        $overall = "Bad";
    } elseif ($score < 20)
    {
        $overall = "Not Bad";
    } elseif ($score < 30)
    {
        $overall = "Okay";
    } elseif ($score < 40)
    {
        $overall = "Average";
    } else
    {
        $overall = "Good";
    }
    $query = "SELECT * FROM sitemap_options";
    $result = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($result))
    {
        $priority = Trim($row['priority']);
        $changefreq = Trim($row['changefreq']);
    }

    $query = mysqli_query($con, "SELECT * FROM sitemap WHERE site='$site'");

    if (mysqli_num_rows($query) > 0)
    {
        //domain exists
    } else
    {
        //domain not exists
        $c_date = date('Y-m-d');
        $sitemap_des = "This web site has Google PageRank $go_rank out of 10 maxmium and is " .
            strtolower($dmoz) . " in DMOZ. The age of $site is $age. Overall site looking $overall. According to the Alexa.com, this site has $alexa rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.";
        $xquery = "INSERT INTO sitemap (site,des,date) VALUES ('$site','$sitemap_des','$c_date')";
        mysqli_query($con, $xquery);
        $site_data = file_get_contents("sitemap.xml");
        $site_data = str_replace("</urlset>", "", $site_data);
        $server_name = "http://" . $_SERVER['SERVER_NAME'] . "/" . $site;
        $c_sitemap = '
 <url>
        <loc>' . $server_name . '</loc>
        <priority>' . $priority . '</priority>
        <changefreq>' . $changefreq . '</changefreq>
        <lastmod>' . $c_date . '</lastmod>
</url>
</urlset>';
        $full_map = $site_data . $c_sitemap;
        file_put_contents("sitemap.xml", $full_map);
    }

?>  
 <style>   #c_alert1{ display:none; } #c_alert2{ display:none; }</style>
 <script>
function contactDoc()
{
var xmlhttp;
var user_name = $('input[name=c_name]').val();
var user_email = $('input[name=c_email]').val();
var user_sub = $('input[name=c_subject]').val();
var user_mes = $('textarea[name=email_message]').val();
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    }
  }
$.post("message.php", {name:user_name,email:user_email,subject:user_sub,message:user_mes}, function(results){
if (results == 1) {
     $("#c_alert1").show();
}
else
{
     $("#c_alert2").show();
}
});
}
</script>  
      <?php

    //include('core/header.php');


?>

        <div class="wrapper">                     
                 <div class="home">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h1 class="animated fadeInDown delay-1">
                                <?php

    echo ucfirst($site);

?>
                            </h1>

<p>        
<?php

    echo $lang['41'];

?> <?php

    echo $alexa;

?> <?php

    echo $lang['42'];

?> 
<?php

    echo $lang['43'];

?> 
<?php

    echo $lang['44'];

?> <?php

    echo $go_rank;

?> <?php

    echo $lang['45'];

?> <?php

    echo strtolower($dmoz);

?> <?php

    echo $lang['46'];

?> <?php

    echo $site;

?> 
is <?php

    echo $age;

?>. <?php

    echo $lang['47'];

?> <?php

    echo "$overall";

?>.
         
 </p>
<br /><br />
                                 

                            <div class="animated fadeInUp delay-4" style="text-align: right;"> 
                                <a href="#" class="btn btn-primary">
                                Google PageRank<br /><code><?php

    echo $go_rank;

?></code><br /></a> &nbsp;&nbsp;&nbsp;
                               <a href="#" class="btn btn-success">
Alexa Ranking<br /><code><?php

    echo $alexa;

?></code><br /></a>&nbsp;&nbsp;&nbsp;
                               <a href="#" class="btn btn-vk">
Domain Age<br /><code><?php

    echo $age;

?></code><br /></a>
              <br /><br /><br />
              
              <div style="text-align: right;" >
<a href="index.php" class="btn btn-danger btn-lg"> <i class="fa fa-search"></i> <?php

    echo $lang['49'];

?> </a>&nbsp;&nbsp;
<a href="<?php

    echo "http://" . $wsite;

?>" class="btn btn-warning btn-lg"> <i class="fa fa-share-square-o"></i> <?php

    echo $lang['48'];

?> </a>       
                      
</div>
                            </div>
                        </div><!--./col-md-6 -->
                        <div class="col-sm-6 presentation">
                            <div class="animated slideInRight delay-1">
                                <div class="downloads" id="downloads"> <?php

    echo ucfirst($site);

?></div>
                                
    <?php

?>     
                                <img  class="imagedropshadow" src="<?php

    echo $myimage

?>" alt="<?php

    echo ucfirst($site);

?>"/>
                            </div>
                        </div><!--./col-md-6 -->
                    </div><!-- /.row -->
                </div>
            </div><!-- /.home -->
            

<div id='share_it'>
	<a class='sharetext'><img src='img/share_text.jpg'/><!--[if gte IE 7]><!--></a><!--<![endif]-->

		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<div class='flyout'>
			<ul class='icons'>
<li class="drop-li"><a href="http://www.facebook.com/share.php?u=<?php

    echo "http://" . $_SERVER['SERVER_NAME'] . "/$site";

?>" target="_blank" class="drop-a"><img alt="" src="img/facebook.png"/><b>Share on FB</b><!--[if gte IE 7]><!--></a><!--<![endif]--></li>
<li class="drop-li"><a href="http://twitter.com/share?url=<?php

    echo "http://" . $_SERVER['SERVER_NAME'] . "/$site";

?>" target="_blank" class="drop-a"><img alt="" src="img/twitter.png"/><b>Tweet This</b><!--[if gte IE 7]><!--></a><!--<![endif]--></li>
<li class="drop-li"><a href="http://www.stumbleupon.com/submit?url=<?php

    echo "http://" . $_SERVER['SERVER_NAME'] . "/$site";

?>&title=<?php

    echo ucfirst($site) . " | $msite_name";

?>" target="_blank" class="drop-a"><img alt="" src="img/stumbleupon.png"/><b>Stumble It</b><!--[if gte IE 7]><!--></a><!--<![endif]--></li>
<li class="drop-li"><a href="http://digg.com/submit?phase=2&url=<?php

    echo "http://" . $_SERVER['SERVER_NAME'] . "/$site";

?>&title=<?php

    echo ucfirst($site) . " | $msite_name";

?>" target="_blank" class="drop-a"><img alt="" src="img/digg.png"/><b>Digg This</b><!--[if gte IE 7]><!--></a><!--<![endif]--></li>
<li class="drop-li"><a href="http://delicious.com/post?url1=<?php

    echo "http://" . $_SERVER['SERVER_NAME'] . "/$site";

?>&title=<?php

    echo ucfirst($site) . " | $msite_name";

?>" target="_blank" class="drop-a"><img alt="" src="img/delicious.png"/><b>Delicious</b><!--[if gte IE 7]><!--></a><!--<![endif]--></li>

			</ul>
		</div>
</div>
            
                    <div class="ad animated fadeInDown delay-5">
               
                <div class="container">
                    <div class="row">                        
            
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.ad -->      
            <div class="features-menu">
                <div class="container">
                
<?php

    echo $ads_1;

?>
                
                 </div>           
                     </div>
                     
                     <div class="container">
                     <h4 class="page-header">
<?php

    echo $lang['50'];

?>
</h4>
                             
                            <!-- Success box -->
                            <div class="box box-solid box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php

    echo ucfirst($site);

?> - <?php

    echo $status;

?></h3>
                                    <div class="box-tools pull-right">
                                        <button data-widget="collapse" class="btn btn-primary btn-sm"><i class="fa fa-minus"></i></button>
                                        <button data-widget="remove" class="btn btn-primary btn-sm"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                
                                <div class="box-body">
<div class="row">
                                        <div class="col-md-4">
                                        
                                                    <div class="box-body no-padding">
                                        
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <td><?php

    echo $lang['51'];

?></td>
                                                 <td><code>Site <?php

    echo $status;

?></code></td>
                                        </tr>
                                        <tr>
                                            <td><?php

    echo $lang['52'];

?></td>
                                                <td><code><?php

    echo $vtime;

?> Sec</code></td>
                                        </tr>
                                           <tr>                              
                                            <td><?php

    echo $lang['53'];

?></td>
                                                <td><code><?php

    echo "$overall";

?></code></td>
                                        </tr>
                                            <tr>
                                            <td></td>
                                                <td></td>
                                        </tr>
                                    </tbody></table>
                                </div>
                             
                                        </div><!-- ./col -->

                       
<div class="col-md-4"  style="text-align: center;" ><div><?php

    echo $lang['54'];

?></div> <br />


<div style="display: inline; width: 90px; height: 90px;">
<input type="text" data-readonly="true" data-fgcolor="#00ACD6" data-height="90" data-width="90" value="<?php

    echo $score;

?>" class="knob" readonly="readonly" style="width: 49px; height: 30px; position: absolute; vertical-align: middle; margin-top: 30px; margin-left: -69px; border: 0px none; background: none repeat scroll 0% 0% transparent; font: bold 18px Arial; text-align: center; color: rgb(60, 141, 188); padding: 0px;" />
<script>$('.knob').knob();</script>
</div>



</div><!-- ./col -->

<div class="col-md-4" style="text-align: center;">         
<p><?php

    echo $lang['55'];

?></p>  <br />
<form method="POST" action="gen_pdf.php">
<input type="hidden" name="site" id="site" value="<?php

    echo $site;

?>"/>
<input type="hidden" name="score" value="<?php

    echo $score;

?>"/>
<input type="hidden" name="title" value="<?php

    echo $title;

?>"/>
<input type="hidden" name="description" value="<?php

    echo $description;

?>"/>
<input type="hidden" name="keywords" value="<?php

    echo $keywords;

?>"/>
<input type="hidden" name="alexa" value="<?php

    echo $alexa;

?>"/>
<input type="hidden" name="overall" value="<?php

    echo $overall;

?>"/>
<input type="hidden" name="age" value="<?php

    echo $age;

?>"/>
<input type="hidden" name="go_rank" value="<?php

    echo $go_rank;

?>"/>
<input type="hidden" name="vtime" value="<?php

    echo $vtime;

?>"/>
<input type="hidden" name="status" value="<?php

    echo $status;

?>"/>
<input type="hidden" name="Tweets" value="<?php

    echo $Tweets;

?>"/>
<input type="hidden" name="face_like" value="<?php

    echo $face_like;

?>"/>
<input type="hidden" name="face_share" value="<?php

    echo $face_share;

?>"/>
<input type="hidden" name="face_come" value="<?php

    echo $face_come;

?>"/>
<input type="hidden" name="LinkedIn" value="<?php

    echo $LinkedIn;

?>"/>
<input type="hidden" name="PlusOnes" value="<?php

    echo $PlusOnes;

?>"/>
<input type="hidden" name="Delicious" value="<?php

    echo $Delicious;

?>"/>
<input type="hidden" name="StumbleUpon" value="<?php

    echo $StumbleUpon;

?>"/>
<input type="hidden" name="host_ip" value="<?php

    echo $host_ip;

?>"/>
<input type="hidden" name="host_country" value="<?php

    echo $host_country;

?>"/>
<input type="hidden" name="host_isp" value="<?php

    echo $host_isp;

?>"/>
<input type="hidden" name="goglebacklink" value="<?php

    echo $goglebacklink;

?>"/>
<input type="hidden" name="googleindexp" value="<?php

    echo $googleindexp;

?>"/>
<input type="hidden" name="bingbacklink" value="<?php

    echo $bingbacklink;

?>"/>
<input type="hidden" name="dmoz" value="<?php

    echo $dmoz;

?>"/>
<input type="hidden" name="black_check" value="<?php

    echo $black_check;

?>"/>
<input type="hidden" name="sitemap_check" value="<?php

    echo $sitemap_r;

?>"/>
<input type="hidden" name="robo_check" value="<?php

    echo $robo_r;

?>"/>
<input type="hidden" name="mal_value" value="<?php

    echo $mal_value;

?>"/>
<button class="btn btn-info" type="submit"><i class="fa fa-cloud-download"></i> <?php

    echo $lang['56'];

?></button>
</form>

</div>
                                    </div>
                                    

                                  <div class="box-footer" style="display: block;"><br />

  <table class="table table-hover">
                                        <tbody><tr style="background-color: #F3F4F5;">
                                            <th><?php

    echo $lang['57'];

?></th>
                                            <th><?php

    echo $lang['58'];

?></th>
                                            <th><?php

    echo $lang['59'];

?></th>

                                        </tr>
                                        <tr>
                                            <td><?php

    echo $lang['60'];

?></td>
                                          <td><b class="text-light-blue"><?php

    echo $title;

?></b></td>
<?php

    if ($title == "No Title")
    {
        echo '<td><span class="label label-danger">Bad</span></td>';
    } elseif ($title == "")
    {
        echo '<td><span class="label label-danger">Bad</span></td>';
    } else
    {
        if (strlen($title) > "71")
        {
            echo '<td><span class="label label-danger">Bad</span></td>';

        } elseif (strlen($title) < "10")
        {
            echo '<td><span class="label label-warning">Warning</span></td>';
        } elseif (strlen($title) < "30")
        {
            echo '<td><span class="label label-success">Good</span></td>';
        } else
        {
            echo '<td><span class="label label-success">Good</span></td>';
        }
    }

?>
                                      </tr>
                                         <tr>
                                            <td><?php

    echo $lang['61'];

?></td>
                                            <td><b><?php

    echo $description;

?></b></td>
                                            
                                            <?php

    if ($description == "No Description")
    {
        echo '<td><span class="label label-danger">Bad</span></td>';
    } elseif ($description == "")
    {
        echo '<td><span class="label label-danger">Bad</span></td>';
    } else
    {
        if (strlen($description) > "170")
        {
            echo '<td><span class="label label-danger">Bad</span></td>';


        } elseif (strlen($description) < "10")
        {
            echo '<td><span class="label label-warning">Warning</span></td>';

        } else
        {
            echo '<td><span class="label label-success">Good</span></td>';
        }
    }

?>
                                        </tr>
                                                                            <tr>
                                            <td><?php

    echo $lang['62'];

?></td>
                                            <td><b><?php

    echo $keywords;

?></b></td>

<?php

    if ($keywords == "No Keywords")
    {
        echo '<td><span class="label label-warning">Warning</span></td>';
    } else
    {
        if (str_word_count($keywords) > "6")
        {
            echo '<td><span class="label label-danger">Bad</span></td>';

        } elseif (strlen($keywords) < "1")
        {
            echo '<td><span class="label label-warning">Warning</span></td>';
        } else
        {
            echo '<td><span class="label label-success">Good</span></td>';
        }
    }

?>
                                        </tr>                                      
    </tbody>    
 <tr><td colspan='3' style="background-color: #FFF;">&nbsp;</td></tr>
                                        <tbody><tr  style="background-color: #F3F4F5;">
                                            <th>Crawling files</th>
                                            <th><?php

    echo $lang['84'];

?></th>
                                            <th><?php

    echo $lang['59'];

?></th>

                                        </tr>
                                        
    <tr>
<td>Robot.txt</td>
<td><b><?php

    if ($robo_r == "0")
    {
        echo $lang['63'];
    } else
    {
        echo $lang['64'];
    }

?></b></td>
<?php

    if ($robo_r == "0")
    {
        echo '<td><span class="label label-danger">Bad</span></td>';
    } else
    {
        echo '<td><span class="label label-success">Good</span></td>';
    }

?>
</tr>

<tr>
<td>XML Sitemap</td>
<td><b><?php

    if ($sitemap_r == "0")
    {
        echo $lang['65'];
    } else
    {
        echo $lang['66'];
    }

?></b></td>
<?php

    if ($sitemap_r == "0")
    {
        echo '<td><span class="label label-danger">Bad</span></td>';
    } else
    {
        echo '<td><span class="label label-success">Good</span></td>';
    }

?>
    </tr>                                           
    </tbody>
    
    
 <tr><td colspan='3' style="background-color: #FFF;">&nbsp;</td></tr>
                                        <tbody><tr  style="background-color: #F3F4F5;">
                                            <th><?php

    echo $lang['83'];

?></th>
                                            <th><?php

    echo $lang['84'];

?></th>
                                            <th><?php

    echo $lang['59'];

?></th>

                                        </tr>
                                        
    <tr>
<td>Safe Browsing</td>
<td><b><?php

    if ($mal_value == "200")
    {
        echo $lang['87'];
    } else
    {
        echo $lang['85'];
    }

?></b></td>
<?php

    if ($mal_value == "200")
    {
        echo '<td><span class="label label-danger">Bad</span></td>';
    } else
    {
        echo '<td><span class="label label-success">Good</span></td>';
    }

?>
</tr>

<tr>
<td><?php

    echo $lang['89'];

?></td>
<td><b><?php

    if ($mal_value == "200")
    {
        echo $lang['88'];
    } else
    {
        echo $lang['86'];
    }

?></b></td>
<?php

    if ($mal_value == "200")
    {
        echo '<td><span class="label label-danger">Bad</span></td>';
    } else
    {
        echo '<td><span class="label label-success">Good</span></td>';
    }

?>
    </tr>                                           
    </tbody>    
    
    
    </table>                                           
                                                                           
                               </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                               </div>
                               
                               
                               <br />
                               
                     <div class="container">
                     <h4 class="page-header">
<?php

    echo $lang['67'];

?>
</h4>
  
  
  <div class="row">
                        <div class="col-md-6">

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php

    echo $lang['68'];

?></h3>
                                </div>
                                <div class="box-body">
                    <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <tbody><tr>
                                            <th><?php

    echo $lang['69'];

?></th>
                                            <th><?php

    echo $lang['70'];

?></th>
                                            <th style="width: 40px">Score</th>
                                        </tr>
                                        <tr>
                                            <td>Google BackLinks</td>
                                                 <td><?php

    echo $goglebacklink;

?></td>
                                    
                           <?php

    $goglebacklink_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $goglebacklink);
    if ($goglebacklink_s < 10)
    {
        echo '<td><span class="badge bg-red">10%</span></td>';
    } elseif ($goglebacklink_s < 20)
    {
        echo '<td><span class="badge bg-red">30%</span></td>';
    } elseif ($goglebacklink_s < 40)
    {
        echo '<td><span class="badge bg-red">40%</span></td>';
    } elseif ($goglebacklink_s < 50)
    {
        echo '<td><span class="badge bg-red">60%</span></td>';
    } elseif ($goglebacklink_s < 100)
    {
        echo '<td><span class="badge bg-red">70%</span></td>';
    } elseif ($goglebacklink_s < 200)
    {
        echo '<td><span class="badge bg-red">80%</span></td>';
    } else
    {
        echo '<td><span class="badge bg-red">100%</span></td>';
    }

?>
                                        </tr>
                                        <tr>
                                            <td>Google Indexed Pages</td>
                                                <td><?php

    echo $googleindexp;

?></td>
                  <?php

    $googleindexp_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $googleindexp);
    if ($googleindexp_s < 20)
    {
        echo '<td><span class="badge bg-yellow">10%</span></td>';
    } elseif ($googleindexp_s < 30)
    {
        echo '<td><span class="badge bg-yellow">30%</span></td>';
    } elseif ($googleindexp_s < 50)
    {
        echo '<td><span class="badge bg-yellow">50%</span></td>';
    } elseif ($googleindexp_s < 100)
    {
        echo '<td><span class="badge bg-yellow">60%</span></td>';
    } elseif ($googleindexp_s < 200)
    {
        echo '<td><span class="badge bg-yellow">70%</span></td>';
    } elseif ($googleindexp_s < 400)
    {
        echo '<td><span class="badge bg-yellow">80%</span></td>';
    } else
    {
        echo ' <td><span class="badge bg-yellow">100%</span></td>';
    }

?>

                                        </tr>
                                        <tr>
                                            <td>Bing BackLinks</td>
                                                <td><?php

    echo $bingbacklink;

?></td>
                                          <?php

    $bingbacklink_s = str_replace(array(
        ",",
        ".",
        ":",
        "'"), "", $bingbacklink);
    if ($bingbacklink_s < 5)
    {
        echo '<td><span class="badge bg-blue">10%</span></td>';
    } elseif ($bingbacklink_s < 15)
    {
        echo '<td><span class="badge bg-blue">30%</span></td>';
    } elseif ($bingbacklink_s < 30)
    {
        echo '<td><span class="badge bg-blue">40%</span></td>';
    } elseif ($bingbacklink_s < 40)
    {
        echo '<td><span class="badge bg-blue">60%</span></td>';
    } elseif ($bingbacklink_s < 50)
    {
        echo '<td><span class="badge bg-blue">70%</span></td>';
    } elseif ($bingbacklink_s < 100)
    {
        echo '<td><span class="badge bg-blue">80%</span></td>';
    } else
    {
        echo '<td><span class="badge bg-blue">100%</span></td>';
    }

?>      

                                        </tr>
                                        <tr>
                                            <td>Dmoz</td>
                                               <td><?php

    echo $dmoz;

?></td>
                                               
                                               <?php

    if ($dmoz == "Listed")
    {
        echo '<td><span class="badge bg-green">100%</span></td>';
    } else
    {
        echo '<td><span class="badge bg-green">0%</span></td>';
    }

?>
                                        </tr>
                                    </tbody></table>
                                </div><br />

                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <div class="box box-success">
                                <div class="box-header">
                                    <h3 class="box-title"><?php

    echo $lang['71'];

?></h3>
                                </div>
                                <div class="box-body">

  <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <td><?php

    echo $lang['72'];

?></td>
                                            <td> <span class="badge bg-green"><?php

    echo $host_ip;

?></span></td>

                                        </tr>
                                         <tr>
                                            <td><?php

    echo $lang['73'];

?></td>
                                            <td><b><span class="badge bg-light-blue"><?php

    echo $host_country;

?></span></b></td>

                                        </tr>
                                            <tr>
                                            <td><?php

    echo $lang['74'];

?></td>
                                            <td><span class="badge bg-red"><?php

    echo $host_isp;

?></span></td>
                                        </tr>
                                     <tr>
              <td></td>          <td></td>
                                        </tr>
                                    </tbody></table>                          
                                    
                                    
                                    <br />
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->

                            <div class="box box-danger">
                                <div class="box-header">
                                    <h3 class="box-title"><?php

    echo $lang['75'];

?> </h3>
                                </div>
                                <div class="box-body">
 <?php

    echo $black_check;

?> <br /><br />
 <?php

    echo $lang['76'];

?><br /><br />
                              </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            
                        </div><!-- /.col (LEFT) -->
                        <div class="col-md-6">

                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title"><?php

    echo $lang['77'];

?></h3>
                                </div>
                                <div class="box-body chart-responsive">
<div class="form-group">
<textarea placeholder="WHOIS Information..." rows="32" class="form-control" readonly=""><?php

    echo $whois_data;

?></textarea>
                                        </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->


                        </div><!-- /.col (RIGHT) -->
                    </div>                        



</div>    
                             <div class="container">
                     <h4 class="page-header">
 <?php

    echo $lang['78'];

?>
</h4>
        
        <section class="content">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        <?php

    echo $face_like;

?> 
                                    </h3>
                                    <p>
                                        Facebook Likes
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-facebook"></i>
                                </div>
                    
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        <?php

    echo $face_share;

?>
                                    </h3>
                                    <p>
                                       Facebook Share
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-facebook"></i>
                                </div>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-blue">
                                <div class="inner">
                                    <h3>
                                        <?php

    echo $face_come;

?>
                                    </h3>
                                    <p>
                                        Facebook Comments
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-facebook"></i>
                                </div>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        <?php

    echo $PlusOnes;

?>
                                    </h3>
                                    <p>
                                        PlusOnes
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-google-plus"></i>
                                </div>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        <?php

    echo $Delicious;

?>
                                    </h3>
                                    <p>
                                        Pinterest
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-pinterest"></i>
                                </div>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-purple">
                                <div class="inner">
                                    <h3>
                                       <?php

    echo $StumbleUpon;

?>
                                    </h3>
                                    <p>
                                        StumbleUpon
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-stumbleupon"></i>
                                </div>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-teal">
                                <div class="inner">
                                    <h3>
                                        <?php

    echo $Tweets;

?>
                                    </h3>
                                    <p>
                                        Tweets
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-twitter"></i>
                                </div>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-maroon">
                                <div class="inner">
                                    <h3>
                                        <?php

    echo $LinkedIn;

?>
                                    </h3>
                                    <p>
                                        LinkedIn
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-linkedin"></i>
                                </div>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                                      

                </section>
                
                            
     </div>   
     
     
<div class="container">
<h4 class="page-header">

</h4>
                              <div class="features-menu">
                <div class="container">
                
<?php

    echo $ads_2;

?>
                
                 </div>           
                     </div>
 </div>    
                                     
<?php

    include ('core/footer.php');

?>

        </div><!--/.wrapper -->

        <!-- jQuery 1.10.2 -->
        <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <!-- Latest compiled and minified Bootstrap JavaScript -->
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
        
 <!-- COMPOSE MESSAGE MODAL -->
        <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Contact US</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                        <div id="c_alert1">
                              <div class="alert alert-success alert-dismissable">
       <i class="fa fa-check"></i>
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
       <b><?php

    echo $lang['21'];

?>!</b> <?php

    echo $lang['22'];

?>
       </div>
                        </div>
                        <div id="c_alert2">
                                <div class="alert alert-danger alert-dismissable">
       <i class="fa fa-ban"></i>
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         <b><?php

    echo $lang['21'];

?></b> <?php

    echo $lang['23'];

?>
        </div>
                        </div>
                        <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Name:</span>
                                    <input name="c_name" id="c_name" type="text" class="form-control" placeholder="<?php

    echo $lang['24'];

?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Email ID:</span>
                                    <input name="c_email" id="c_email" type="email" class="form-control" placeholder="<?php

    echo $lang['25'];

?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Subject:</span>
                                    <input name="c_subject" id="c_subject" type="email" class="form-control" placeholder="<?php

    echo $lang['26'];

?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="email_message" id="email_message" class="form-control" placeholder="<?php

    echo $lang['27'];

?>" style="height: 120px;"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php

    echo $lang['28'];

?></button>

                            <button type="button" onclick="contactDoc()" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> <?php

    echo $lang['29'];

?></button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
<?php

} else
{

    echo '
    
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="Turbo SEO Analyser"/>
        <meta property="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />
        <meta name="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />

        <title>500 Error - Turbo SEO Analyser</title>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        500 Error!
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">500 error</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline">500</h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! Something went wrong.</h3>
                            <p>
                                We will work on fixing that right away. 
                                Meanwhile, you may <a href="index.php">return to index page</a>.
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';

}
off : if ($my_site == "0")
{
    echo '
    
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="Turbo SEO Analyser"/>
        <meta property="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />
        <meta name="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />

        <title>Offline Site - Turbo SEO Analyser</title>
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Turbo SEO Analyser
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Offline Site</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! Your requested site is offline.</h3>
                            <p>
                                Try to fix your site soon. 
                                New search, you may <a href="index.php">return to index page</a>.
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';
}

?>
<?php

mysqli_close($con);

?>

<?php

myoff : if ($check_site == "0")
{
    echo ' 
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="Turbo SEO Analyser"/>
        <meta property="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />
        <meta name="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />

        <title>Offline Site - Turbo SEO Analyser</title>
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Turbo SEO Analyser
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Offline Site</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! SQL ERROR: ' .
        $sql_error . '</h3>
                            <p>
                                Try to fix your site soon. 
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';
}
banned_user : if ($ban_user == "0")
{
    echo ' 
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="' . $site_name . '"/>
        <meta property="description" content="' . $des . '" />
        <meta name="description" content="' . $des . '" />

        <title>' . $lang['30'] . ' - ' . $title . '</title>
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        ' . $site_name . '
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> ' .
        $lang['31'] . '</a></li>
                        <li class="active">' . $lang['30'] . '</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> ' . $lang['32'] .
        '</h3>
                            <p>
                                ' . $lang['33'] . '<br>
                                ' . $lang['34'] . ' (' . $ip . ').  
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';
}
banned_site : if ($ban_site == "0")
{
    echo ' 
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="' . $site_name . '"/>
        <meta property="description" content="' . $des . '" />
        <meta name="description" content="' . $des . '" />

        <title>' . $lang['35'] . ' - ' . $title . '</title>
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        ' . $site_name . '
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> ' .
        $lang['31'] . '</a></li>
                        <li class="active">' . $lang['35'] . '</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                          <h3><i class="fa fa-warning text-yellow"></i> ' . $lang['32'] .
        '</h3>
                            <p>
                                ' . $lang['33'] . '<br>
                                ' . $lang['36'] . ' (' . $site . ').  
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';
}

?>