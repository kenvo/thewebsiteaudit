

Note: 
i) Backup your files before upgrade.
ii) This upgrade have some changes on interface part. So you're theme customizations may be affected.


Updation Steps:

1. Goto this folder: "Upgrade/v1.4 to v1.5/Upload"

2. Use your Server FTP or File Manager application on your server. 

3. Now, replace the old files directly on your server with new files present upload directory.

4. After successful file upload, just visit the "upgrade.php" file on your browser and complete your updation.

"http://your-domain.com/upgrade.php"

You have done the work! 