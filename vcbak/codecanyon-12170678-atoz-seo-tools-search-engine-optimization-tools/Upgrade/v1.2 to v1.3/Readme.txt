
How to upgrade from v1.2 to v1.3 ?

1. Delete this folder from your server completely

  i)  ->  "/admin/theme/default/plugins/ckeditor/"

  ii) ->  "/core/library/filemanager/"


2. Download the new ZIP package from Codecanyon.

3. Goto this folder: "Upgrade/v1.2 to v1.3/Upload"

4. Now, replace the old files directly on your server with new files present on upgrade folder.

You have done the work! 