-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 06 Août 2016 à 03:52
-- Version du serveur :  5.6.25
-- Version de PHP :  5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `simple-audit`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `user` varchar(250) DEFAULT NULL,
  `pass` varchar(250) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`id`, `user`, `pass`) VALUES
(1, 'admin', 'da37702156462803b5af89ac6a85dce5'),
(2, 'admin', 'da37702156462803b5af89ac6a85dce5'),
(3, 'admin', 'da37702156462803b5af89ac6a85dce5'),
(4, 'admin', 'da37702156462803b5af89ac6a85dce5'),
(5, 'admin', 'da37702156462803b5af89ac6a85dce5');

-- --------------------------------------------------------

--
-- Structure de la table `admin_history`
--

CREATE TABLE IF NOT EXISTS `admin_history` (
  `id` int(11) NOT NULL,
  `last_date` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admin_history`
--

INSERT INTO `admin_history` (`id`, `last_date`, `ip`) VALUES
(1, '14th June 2014', '23.54.2.43'),
(2, '14th June 2014', '26.32.34.33'),
(3, '15th June 2014', '31.7.42.03'),
(4, '14th June 2014', '23.54.2.43'),
(5, '14th June 2014', '26.32.34.33'),
(6, '15th June 2014', '31.7.42.03'),
(7, '14th June 2014', '23.54.2.43'),
(8, '14th June 2014', '26.32.34.33'),
(9, '15th June 2014', '31.7.42.03'),
(10, '14th June 2014', '23.54.2.43'),
(11, '14th June 2014', '26.32.34.33'),
(12, '15th June 2014', '31.7.42.03'),
(13, '14th June 2014', '23.54.2.43'),
(14, '14th June 2014', '26.32.34.33'),
(15, '15th June 2014', '31.7.42.03'),
(16, '4th August 2016', '127.0.0.1');

-- --------------------------------------------------------

--
-- Structure de la table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(255) DEFAULT NULL,
  `text_ads` mediumtext,
  `ads_1` mediumtext,
  `ads_2` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ads`
--

INSERT INTO `ads` (`id`, `text_ads`, `ads_1`, `ads_2`) VALUES
(1, '<br />\r\nTry Pro IP locator Script Today! CLICK HERE <br /><br />\r\n\r\nGet 20,000 Unique Traffic for $5 [Limited Time Offer] - Buy Now! CLICK HERE<br /><br />\r\n\r\nCustom OpenVPN GUI - Get Now for $15 ! CLICK HERE<br />', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center> ', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center>'),
(1, '<br />\r\nTry Pro IP locator Script Today! CLICK HERE <br /><br />\r\n\r\nGet 20,000 Unique Traffic for $5 [Limited Time Offer] - Buy Now! CLICK HERE<br /><br />\r\n\r\nCustom OpenVPN GUI - Get Now for $15 ! CLICK HERE<br />', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center> ', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center>'),
(1, '<br />\r\nTry Pro IP locator Script Today! CLICK HERE <br /><br />\r\n\r\nGet 20,000 Unique Traffic for $5 [Limited Time Offer] - Buy Now! CLICK HERE<br /><br />\r\n\r\nCustom OpenVPN GUI - Get Now for $15 ! CLICK HERE<br />', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center> ', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center>'),
(1, '<br />\r\nTry Pro IP locator Script Today! CLICK HERE <br /><br />\r\n\r\nGet 20,000 Unique Traffic for $5 [Limited Time Offer] - Buy Now! CLICK HERE<br /><br />\r\n\r\nCustom OpenVPN GUI - Get Now for $15 ! CLICK HERE<br />', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center> ', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center>'),
(1, '<br />\r\nTry Pro IP locator Script Today! CLICK HERE <br /><br />\r\n\r\nGet 20,000 Unique Traffic for $5 [Limited Time Offer] - Buy Now! CLICK HERE<br /><br />\r\n\r\nCustom OpenVPN GUI - Get Now for $15 ! CLICK HERE<br />', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center> ', '<center>\r\n <div >\r\n<img class="imageres" src="img/ad700.png">\r\n</div>  <br /> <br /> </center>');

-- --------------------------------------------------------

--
-- Structure de la table `ban_site`
--

CREATE TABLE IF NOT EXISTS `ban_site` (
  `id` int(11) NOT NULL,
  `site` varchar(255) DEFAULT NULL,
  `last_date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ban_site`
--

INSERT INTO `ban_site` (`id`, `site`, `last_date`) VALUES
(1, 'example.com', '17th June 2014'),
(2, 'prothemes.biz', '17th June 2014');

-- --------------------------------------------------------

--
-- Structure de la table `ban_user`
--

CREATE TABLE IF NOT EXISTS `ban_user` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `last_date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ban_user`
--

INSERT INTO `ban_user` (`id`, `ip`, `last_date`) VALUES
(1, '2.2.2.2', '17th June 2014');

-- --------------------------------------------------------

--
-- Structure de la table `lang`
--

CREATE TABLE IF NOT EXISTS `lang` (
  `id` int(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `lang`
--

INSERT INTO `lang` (`id`, `type`) VALUES
(0, 'en.php'),
(0, 'en.php'),
(0, 'en.php'),
(0, 'en.php'),
(0, 'en.php');

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL,
  `last_date` varchar(255) DEFAULT NULL,
  `page_name` varchar(255) DEFAULT NULL,
  `page_title` mediumtext,
  `page_content` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`id`, `last_date`, `page_name`, `page_title`, `page_content`) VALUES
(1, '17th June 2014', 'About', 'About US', '<p><strong>Nothing to say</strong></p><br><br><br><br><br><br><br><br><br><br>');

-- --------------------------------------------------------

--
-- Structure de la table `page_view`
--

CREATE TABLE IF NOT EXISTS `page_view` (
  `id` int(11) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `tpage` varchar(255) DEFAULT NULL,
  `tvisit` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `page_view`
--

INSERT INTO `page_view` (`id`, `date`, `tpage`, `tvisit`) VALUES
(1, '14th June 2014', '9', '1'),
(2, '14th June 2014', '9', '1'),
(3, '15th June 2014', '14', '1'),
(4, '15th June 2014', '14', '1'),
(5, '14th June 2014', '9', '1'),
(6, '15th June 2014', '14', '1'),
(7, '14th June 2014', '9', '1'),
(8, '15th June 2014', '14', '1'),
(9, '14th June 2014', '9', '1'),
(10, '15th June 2014', '14', '1'),
(11, '4th August 2016', '1', '1'),
(12, '5th August 2016', '78', '1');

-- --------------------------------------------------------

--
-- Structure de la table `sitemap`
--

CREATE TABLE IF NOT EXISTS `sitemap` (
  `id` int(11) NOT NULL,
  `site` text,
  `des` text,
  `date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sitemap`
--

INSERT INTO `sitemap` (`id`, `site`, `des`, `date`) VALUES
(1, 'prothemes.biz', 'This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04'),
(2, 'prothemes.biz', 'This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04'),
(3, 'prothemes.biz', 'This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04'),
(4, 'google.com', 'This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04'),
(5, 'google.com', 'This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04'),
(6, 'google.com', 'This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04'),
(7, 'prothemes.biz', 'This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04'),
(8, 'google.com', 'This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04'),
(9, 'prothemes.biz', 'This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04'),
(10, 'google.com', 'This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.', '2014-08-04');

-- --------------------------------------------------------

--
-- Structure de la table `sitemap_options`
--

CREATE TABLE IF NOT EXISTS `sitemap_options` (
  `id` int(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `changefreq` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sitemap_options`
--

INSERT INTO `sitemap_options` (`id`, `priority`, `changefreq`) VALUES
(1, '0.9', 'weekly'),
(1, '0.9', 'weekly'),
(1, '0.9', 'weekly'),
(1, '0.9', 'weekly'),
(1, '0.9', 'weekly');

-- --------------------------------------------------------

--
-- Structure de la table `site_info`
--

CREATE TABLE IF NOT EXISTS `site_info` (
  `id` int(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `des` mediumtext,
  `keyword` mediumtext,
  `site_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `twit` varchar(4000) DEFAULT NULL,
  `face` varchar(4000) DEFAULT NULL,
  `gplus` varchar(4000) DEFAULT NULL,
  `ex_1` mediumtext,
  `ex_2` mediumtext,
  `ps_1` mediumtext,
  `ps_2` mediumtext,
  `ps_3` mediumtext,
  `ga` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `site_info`
--

INSERT INTO `site_info` (`id`, `title`, `des`, `keyword`, `site_name`, `email`, `twit`, `face`, `gplus`, `ex_1`, `ex_2`, `ps_1`, `ps_2`, `ps_3`, `ga`) VALUES
(0, 'Turbo SEO Analyzer - All-in-one SEO Tool', 'Analyze your web pages with our Turbo SEO Analyzer and Build SEO reports as PDF.', 'seo,analyzer,woorank,alexa,google', 'Turbo SEO Analyzer', 'admin@prothemes.biz', 'https://twitter.com/', 'https://www.facebook.com/', 'https://plus.google.com/', 'Google.com', 'ProThemes.biz', 'google.com', 'prothemes.biz', 'codecanyon.net', 'UA-'),
(0, 'Turbo SEO Analyzer - All-in-one SEO Tool', 'Analyze your web pages with our Turbo SEO Analyzer and Build SEO reports as PDF.', 'seo,analyzer,woorank,alexa,google', 'Turbo SEO Analyzer', 'admin@prothemes.biz', 'https://twitter.com/', 'https://www.facebook.com/', 'https://plus.google.com/', 'Google.com', 'ProThemes.biz', 'google.com', 'prothemes.biz', 'codecanyon.net', 'UA-'),
(0, 'Turbo SEO Analyzer - All-in-one SEO Tool', 'Analyze your web pages with our Turbo SEO Analyzer and Build SEO reports as PDF.', 'seo,analyzer,woorank,alexa,google', 'Turbo SEO Analyzer', 'admin@prothemes.biz', 'https://twitter.com/', 'https://www.facebook.com/', 'https://plus.google.com/', 'Google.com', 'ProThemes.biz', 'google.com', 'prothemes.biz', 'codecanyon.net', 'UA-'),
(0, 'Turbo SEO Analyzer - All-in-one SEO Tool', 'Analyze your web pages with our Turbo SEO Analyzer and Build SEO reports as PDF.', 'seo,analyzer,woorank,alexa,google', 'Turbo SEO Analyzer', 'admin@prothemes.biz', 'https://twitter.com/', 'https://www.facebook.com/', 'https://plus.google.com/', 'Google.com', 'ProThemes.biz', 'google.com', 'prothemes.biz', 'codecanyon.net', 'UA-'),
(0, 'Turbo SEO Analyzer - All-in-one SEO Tool', 'Analyze your web pages with our Turbo SEO Analyzer and Build SEO reports as PDF.', 'seo,analyzer,woorank,alexa,google', 'Turbo SEO Analyzer', 'admin@prothemes.biz', 'https://twitter.com/', 'https://www.facebook.com/', 'https://plus.google.com/', 'Google.com', 'ProThemes.biz', 'google.com', 'prothemes.biz', 'codecanyon.net', 'UA-');

-- --------------------------------------------------------

--
-- Structure de la table `user_history`
--

CREATE TABLE IF NOT EXISTS `user_history` (
  `id` int(11) NOT NULL,
  `last_date` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_history`
--

INSERT INTO `user_history` (`id`, `last_date`, `ip`, `site`) VALUES
(1, '14th June 2014', '23.54.72.77', 'prothemes.biz'),
(2, '14th June 2014', '23.54.72.77', 'prothemes.biz'),
(3, '14th June 2014', '23.54.72.77', 'prothemes.biz'),
(4, '14th June 2014', '23.54.72.77', 'prothemes.biz'),
(5, '14th June 2014', '23.54.72.77', 'bluepay.com'),
(6, '14th June 2014', '23.54.72.77', 'bluepay.com'),
(7, '14th June 2014', '23.54.72.77', 'bluepay.com'),
(8, '14th June 2014', '23.54.72.77', 'bluepay.com'),
(9, '14th June 2014', '23.54.72.77', 'codecanyon.net'),
(10, '14th June 2014', '23.54.72.77', 'codecanyon.net'),
(11, '14th June 2014', '23.54.72.77', 'codecanyon.net'),
(12, '14th June 2014', '23.54.72.77', 'codecanyon.net'),
(13, '14th June 2014', '23.54.72.77', 'mobikwik.com'),
(14, '14th June 2014', '23.54.72.77', 'mobikwik.com'),
(15, '14th June 2014', '23.54.72.77', 'mobikwik.com'),
(16, '14th June 2014', '23.54.72.77', 'mobikwik.com'),
(17, '14th June 2014', '23.54.72.77', 'interserver.net'),
(18, '14th June 2014', '23.54.72.77', 'interserver.net'),
(19, '14th June 2014', '23.54.72.77', 'interserver.net'),
(20, '14th June 2014', '23.54.72.77', 'stackoverflow.com'),
(21, '14th June 2014', '23.54.72.77', 'interserver.net'),
(22, '14th June 2014', '23.54.72.77', 'stackoverflow.com'),
(23, '14th June 2014', '23.54.72.77', 'stackoverflow.com'),
(24, '14th June 2014', '23.54.72.77', 'google.com'),
(25, '14th June 2014', '23.54.72.77', 'stackoverflow.com'),
(26, '14th June 2014', '23.54.72.77', 'google.com'),
(27, '14th June 2014', '23.54.72.77', 'google.com'),
(28, '14th June 2014', '23.54.72.77', 'google.com'),
(29, '14th June 2014', '23.54.72.77', 'prothemes.biz'),
(30, '14th June 2014', '23.54.72.77', 'bluepay.com'),
(31, '14th June 2014', '23.54.72.77', 'codecanyon.net'),
(32, '14th June 2014', '23.54.72.77', 'mobikwik.com'),
(33, '14th June 2014', '23.54.72.77', 'interserver.net'),
(34, '14th June 2014', '23.54.72.77', 'stackoverflow.com'),
(35, '14th June 2014', '23.54.72.77', 'google.com'),
(36, '4th August 2016', '127.0.0.1', 'google.com'),
(37, '5th August 2016', '127.0.0.1', 'google.com'),
(38, '5th August 2016', '127.0.0.1', 'google.com'),
(39, '5th August 2016', '127.0.0.1', 'google.com'),
(40, '5th August 2016', '127.0.0.1', 'google.com'),
(41, '5th August 2016', '127.0.0.1', 'google.com'),
(42, '5th August 2016', '127.0.0.1', 'google.com'),
(43, '5th August 2016', '127.0.0.1', 'google.com'),
(44, '5th August 2016', '127.0.0.1', 'google.com'),
(45, '5th August 2016', '127.0.0.1', 'google.com'),
(46, '5th August 2016', '127.0.0.1', 'google.com'),
(47, '5th August 2016', '127.0.0.1', 'google.com'),
(48, '5th August 2016', '127.0.0.1', 'google.com'),
(49, '5th August 2016', '127.0.0.1', 'google.com'),
(50, '5th August 2016', '127.0.0.1', 'google.com'),
(51, '5th August 2016', '127.0.0.1', 'google.com'),
(52, '5th August 2016', '127.0.0.1', 'google.com'),
(53, '5th August 2016', '127.0.0.1', 'google.com'),
(54, '5th August 2016', '127.0.0.1', 'google.com');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `admin_history`
--
ALTER TABLE `admin_history`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ban_site`
--
ALTER TABLE `ban_site`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ban_user`
--
ALTER TABLE `ban_user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `page_view`
--
ALTER TABLE `page_view`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sitemap`
--
ALTER TABLE `sitemap`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_history`
--
ALTER TABLE `user_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `admin_history`
--
ALTER TABLE `admin_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `ban_site`
--
ALTER TABLE `ban_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `ban_user`
--
ALTER TABLE `ban_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `page_view`
--
ALTER TABLE `page_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `sitemap`
--
ALTER TABLE `sitemap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `user_history`
--
ALTER TABLE `user_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
